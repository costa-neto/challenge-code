// https://docs.cypress.io/api/introduction/api.html

describe('Rotina de Testes para a Todo List', () => {
  it('Visita a Home', () => {
    cy.visit('/')
    cy.get('.headline').contains('Todo App')
  }),
  it('Insere alguns Todos', () => {
    cy.get('#newTodo').click().type('Academia{enter}')
    cy.wait(2000)
    cy.get('#newTodo').click().type('Fazer compras{enter}')
    cy.wait(2000)
    cy.get('#newTodo').click().type('Limpar a casa{enter}')
  }),
  it('Edita um todo', () => {
    cy.wait(2000)
    cy.get('.v-list-item__content').contains('Academia').dblclick()
    cy.get('.inputEdit').type(' dentro de casa{enter}')
    cy.contains('Academia dentro de casa')
  })
})
