import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './stores/store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

const api = axios.create({
  baseURL: "http://localhost/challenge-code/api/"
})


const axiosPlugin = {
  install(Vue){
    Vue.prototype.$api= api;
  }
}

Vue.prototype.$eventBus = new Vue();
Vue.use(axiosPlugin);
new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
