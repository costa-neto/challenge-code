import axios from 'axios';

export const http = axios.create({
    baseURL: 'http://localhost/challenge-code/api/'
});
