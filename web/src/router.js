import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Sobre from './views/Sobre.vue'

Vue.use(Router)

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    //   props: true
    },
    {
      path: '/sobre',
      name: 'sobre',
      component: Sobre,
    //   props: true
    }
  ]
})

export default router