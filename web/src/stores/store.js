import Vue from 'vue';
import Vuex from 'vuex';
import todos from './modules/todos';


Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        appReady:false
    },
    mutations: {
        READY_APP(state)
        {
            state.appReady = true;
        }
    },
    actions: {
        INIT_APP({commit})
        {
            setTimeout(function (){
                commit("READY_APP");
            },3000)
        }
    },
    modules: {
        todos
    }
})