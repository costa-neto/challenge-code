export default({
    namespaced: true,
    state: {
        todos: []
    },
    getters: {
        todos(state) {
            return state.todos;
        },
        GET_TODO: state => id => 
            state.todos.filter(todo => todo.id == id)[0]
    },
    mutations: {
        SET_TODOS(state, todos) {
            state.todos = todos;
        },
        addTodo (state, todo) {
            console.log('adic todo', todo);
            state.todos.push(todo)
          },
          removeTodo (state, todo) {
            console.log('chegou no remove', todo);
            let index=Array.from(state.todos).indexOf(Array.from(todo))
            Array.from(state.todos).splice(index, 1)
          },
          editTodo (state, todo) {
            console.log('updated', todo);
          }
    },
    actions: {
        LOAD_TODOS({commit}, api){
            api.get("getAllTodo/")
            .then(res => {
                commit("SET_TODOS",res.data);
            })
        },
        CREATE_TODO({ commit }, payload){
            let {api, form} = payload;
            console.log(payload, api, form);
            return api.post("createTodo/", form).then(res => {
                if(res.data.message === 'todo Created'){
                    let todo = {
                        todo_title:form.todo_title,
                        todo_done:0
                    }
                 console.log('response certa', form);
                commit("addTodo", todo);
                }
            }).catch(error => {
                    console.log(error.response)
              });
        },
        PUT_TODO({commit}, payload) {
            let {api, todo} = payload;
            console.log('caiu no update\n',"updateTodo/"+todo.id+"/", todo)
            api.put("updateTodo/"+todo.id+"/", todo).then((response) => {
                if(response.data.message === 'todo Updated'){
                    commit("editTodo", todo);
                }
            })
        },
        COMPLETE_TODO({commit}, payload) {
            let {api, todo} = payload;
            console.log('caiu no complete\n',"completeTodo/"+todo.id+"/", todo)
            
            api.put("completeTodo/"+todo.id+"/", todo).then((response) => {
                if(response.data.message === 'todo Updated'){
                    console.log('fez o update')
                    commit("editTodo", todo);
                }
            })
        },
        DELETE_TODO({commit}, payload) {
            let {api, todo} = payload;
            console.log(todo)
            api.delete("deleteTodo/"+todo.id).then((response) => {
                if(response.data.message === 'todo deleted'){
                    commit("removeTodo", todo);
                }
            })
        },
    }
})