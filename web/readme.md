# Web - Challenge CODE 49

Neste diretório deverá ser desenvolvida a **1ª Parte** do desafio.

## Objetivo

O objetivo desta **1ª Parte** é:

- Desenvolver toda a parte visual e funcional do TodoList;
- Criar uma aplicação SPA utilizando VueJs;
- Integrar esta página com a API que será desenvolvida na **2ª Parte**;
- Tratar os possíveis erros ou retornos da API, visando sempre a experiência do usuário;
- Utilizar conceitos de componentização.

## Observações

Algumas observações:

- A estrutura de diretórios desse projeto já foi criada com a utilização do [**Vue-CLI**](https://cli.vuejs.org/)
- Foi instalado no projeto o [**Vuetify**](https://vuetifyjs.com), que deverá ser utilizado no desenvolvimento da parte visual.
- Mantenha o código o mais limpo e intuitivo possível.

## Build Setup

### Install dependencies:
    npm install # Or yarn install

### Spin up server with hot reload (npm run / yarn)
    npm run serve