# Challenge Project - CODE 49

Projeto utilizado para analisar o nível de conhecimento e padrões de código utilizados pelos candidatos para a vaga de Programador Full Stack - Web.

## Entrega

    Para reproduzir o ambiente em que desenvolvi, coloquei a pasta challenge-code nos htdocs do Xampp;
    É necessário mudar os dados de conexão em api/config/Database.php;
    Após o npm install, e colocar o app para rodar com 'npm run serve', é possível rodar uma pequena rotina de testes e2e que criei utilizando Cypress, basta rodar o comando 'npx cypress open' dentro da pasta web e, no GUI do cypress que será aberto selecionar o arquivo test.js;

## Sobre o projeto

O objetivo deste projeto é desenvolver uma TodoList (Lista de Tarefas), onde seja possível:

- Adicionar novos itens a lista;
- Remover itens adicionados;
- Editar itens adicionados;
- Marcar um item como concluído.

Os itens concluídos deverão também ser exibidos separadamente.

O projeto estará dividido em 2 partes:

- **1ª parte:** Página web desenvolvida utilizando VueJs, que será alimentada por uma API.
- **2ª parte:** API desenvolvida utilizando PHP, seguindo o padrão RESTFUL, em conjunto com um banco MYSQL ou MariaDb.

Para o desenvolvimento da API, é possível utilizar o XAMPP, o Docker ou qualquer outra ferramenta que preferir.

A modelagem e criação do Banco deverá ser feito pelo candidato, e o script para criação do banco deverá ser colocado dentro da pasta: [*~/api/database/*](./api/database)

Dentro das pastas [*~/api/*](./api) e [*~/web/*](./web) é possível encontrar instruções mais específicas sobre cada parte do projeto.

## Competências que serão analisadas

- Desenvolvimento de uma aplicação SPA com VueJs.
- Criação e manipulação de um banco de dados Mysql.
- Criação de uma API utilizando PHP.
- Conhecimento no modelo de API RESTFUL.
- Conceitos Models e Controllers.

## Instruções para desenvolvimento e entrega

Antes de começar o desenvolvimento do projeto, de um **Fork** deste repositório para a sua conta pessoal e realize todo o desenvolvimento lá.

Após terminar terminar o projeto, crie uma **Merge Request** do seu repositório pessoal para este repositório principal.

Quando realizar a entrega, informe o recrutador que está em contato com você, e enviaremos os resultados do desafio assim ele for analisado.

## Exemplo

Exemplo para utilizar como referência:

![](./example.gif)