# Api - Challenge CODE 49

Neste diretório deverá ser desenvolvida a **2ª Parte** do desafio.

## Objetivo

O objetivo desta **2ª Parte** é:

- Criar uma rota para cada tipo de interação com a API (mínimo de 4 rotas);
- Seguir os padrões de API RESTFUL;
- Utilizar conceitos Models e Controllers durante o desenvolvimento;
- Adicionar os scripts de criação do Banco de Dados dentro da pasta: [*~/api/database*](./database);
- Documentar de forma simples as rotas criadas. (Rota, finalidade, parâmetros)

## Observações

Algumas observações:

- Não é obrigatório o uso de nenhum framework específico, mas é permitido utilizar algum caso desejado;
- Certifique-se de manter os arquivos de conexão com o Banco organizados e estruturados para facilitar o teste em outros ambientes;
- Não existe nenhuma estrutura de diretórios obrigatória, apenas tente deixar o mais limpo e organizado possível;
- O projeto será testado em um Servidor com Apache, então é permitido o uso de arquivos **.htaccess**;
- Para o desenvolvimento do projeto é recomendado utilizar o [**XAMPP**](https://www.apachefriends.org/pt_br/index.html), ou o [**Docker**](https://www.docker.com/). Caso queira utilizar alguma outra ferramenta, o uso da mesma deverá ser documentado.
- Caso opte pelo uso do [**Docker**](https://www.docker.com/), já foi preparado um docker-compose file com as configurações do ambiente. (Não é obrigatório utiliza-lo)