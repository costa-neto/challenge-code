CREATE DATABASE IF NOT EXISTS todoapp_db;

CREATE TABLE IF NOT EXISTS todo(
id INTEGER PRIMARY KEY NOT NULL AUTO_INCREMENT,
todo_title VARCHAR(80) NOT NULL,
todo_done boolean not null default false,
created_at datetime not null default current_timestamp,
updated_at datetime on update current_timestamp
);