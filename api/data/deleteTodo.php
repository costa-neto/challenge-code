<?php

  include_once './config/Database.php';
  include_once './models/Todo.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Cria novo objeto Todo
  $todo = new Todo($db);

  // Recebe os dados enviados
  $data = json_decode(file_get_contents("php://input"));

  // Define o ID a ser deletado
  $todo->id = $url[1];

  // Deleta TOdo
  if($todo->delete()) {
    echo json_encode(
      array('message' => 'todo deleted')
    );
  } else {
    echo json_encode(
      array('message' => 'todo not deleted')
    );
  }