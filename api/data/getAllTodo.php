<?php 

  include_once './config/Database.php';
  include_once './models/Todo.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Cria novo TOdo
  $todo = new Todo($db);

  // todo read query
  $result = $todo->read();
  
  // Pega o número de linhas retornadas
  $num = $result->rowCount();

  // Check se existem todos
  if($num > 0) {
        // Todo array
        $todo_arr = array();
        $todo_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $todo_item = array(
            'id' => $id,
            'todo_title' => $todo_title,
            'todo_done' => $todo_done,
            'created_at' => $created_at,
            'updated_at' => $updated_at
          );

          // Push to "data"
          array_push($todo_arr['data'], $todo_item);
        }

        // Turn to JSON & output
        echo json_encode($todo_arr);

  } else {
        // No Categories
        echo json_encode(
          array('message' => 'No Todos Found')
        );
  }