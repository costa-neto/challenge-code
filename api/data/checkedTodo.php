<?php

  include_once './config/Database.php';
  include_once './models/Todo.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Cria novo objeto Todo
  $todo = new Todo($db);

  // Recebe o valor enviado
  $data = json_decode(file_get_contents("php://input"));

  // Define as propriedades
  $todo->id = $url[1];
  $todo->todo_done = $data->todo_done;

  // Atualiza o todo
  if($todo->checked()) {
    echo json_encode(
      array('message' => 'todo Updated')
    );
  } else {
    echo json_encode(
      array('message' => 'todo not updated')
    );
  }