<?php

  include_once './config/Database.php';
  include_once './models/Todo.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate novo objeto todo
  $todo = new Todo($db);

  // Recebe os dados enviados
  $data = json_decode(file_get_contents("php://input"));

  $todo->todo_title = $data->todo_title;

  // Create um novo Todo
  if($todo->create()) {
    echo json_encode(
      array('message' => 'todo Created')
    );
  } else {
    echo json_encode(
      array('message' => 'todo Not Created')
    );
  }