<?php

  include_once './config/Database.php';
  include_once './models/Todo.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $todo = new Todo($db);

  // Get raw posted data
  $data = json_decode(file_get_contents("php://input"));

  // Set ID to UPDATE
  $todo->id = $url[1];

  $todo->todo_title = $data->todo_title;

  // Update post
  if($todo->update()) {
    echo json_encode(
      array('message' => 'todo Updated')
    );
  } else {
    echo json_encode(
      array('message' => 'todo not updated')
    );
  }