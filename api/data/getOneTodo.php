<?php


  include_once './config/Database.php';
  include_once './models/Todo.php';
  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();
  // Instantiate blog todo object
  $todo = new Todo($db);

  // Get ID
  $todo->id = isset($_GET['id']) ? $_GET['id'] : die();

  // Get post
  $todo->read_single();

  // Create array
  $todo_arr = array(
    'id' => $todo->id,
    'todo_title' => $todo_title,
    'todo_done' => $todo_done,
    'created_at' => $created_at,
    'updated_at' => $updated_at
  );

  // Make JSON
  print_r(json_encode($todo_arr));