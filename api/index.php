<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Content-type');


//verifica se há uma requisição
if($_REQUEST){

    $url = explode('/', $_REQUEST['url']);

    //rotas disponiveis
    switch($url[0]){

        //lista os todo's
        case 'getAllTodo':
            include("./data/getAllTodo.php");
        break;


        //adiciona um ToDo novo  
        case 'createTodo':
            include("./data/createTodo.php");
        break;

        //Atualiza o ToDo  
        case 'updateTodo':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./data/updateTodo.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;

        //Atualiza o status do ToDo  
        case 'checkedTodo':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./data/checkedTodo.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;


        //deleta o ToDo
        case 'deleteTodo':
            //confirma a existencia do parametro Id para o redirecionamento
            if($url[1]){
                include("./data/deleteTodo.php");
             }else
             echo json_encode(array('status' => 404, 'message' => 'Request Invalid'));
        break;

        //caso o client faça a requisição de uma rota invalida
        default:
            echo json_encode(array('status' => 404, 'message' => 'Route Invalid'));

    }
}

?>
