<?php
  class Todo {
    // Informações do DB
    private $conn;
    private $table = 'todo';

    // Propriedades
    public $id;
    public $todo_title;
    public $todo_done;
    public $created_at;
    public $updated_at;

    // Construtor com o DB
    public function __construct($db) {
      $this->conn = $db;
    }

    // Get TODOS
    public function read() {
      // Cria a query
      $query = 'SELECT *
      FROM
        ' . $this->table . '
      ORDER BY
        created_at DESC';

      // Prepara o statement
      $stmt = $this->conn->prepare($query);

      // Executa a query
      $stmt->execute();

      return $stmt;
    }

    // Get para um TODO específico
  public function read_single(){
    // Cria a query
    $query = 'SELECT *
        FROM
          ' . $this->table . '
      WHERE id = ?
      LIMIT 0,1';

      //Prepara o statement
      $stmt = $this->conn->prepare($query);

      // Define o parâmetro ID
      $stmt->bindParam(1, $this->id);

      // Executa a Query
      $stmt->execute();

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // define as propriedades
      $this->id = $row['id'];
      $this->todo_title = $row['todo_title'];
      $this->todo_title = $row['todo_done'];
      $this->created_at = $row['created_at'];
      $this->updated_at = $row['updated_at'];
  }

  // Create um TODO
  public function create() {
    // Cria a Query
    $query = 'INSERT INTO ' .
      $this->table . '
    SET
      todo_title = :todo_title';

  // Prepara o Statement
  $stmt = $this->conn->prepare($query);


  // Bind para o parâmetro
  $stmt-> bindParam(':todo_title', $this->todo_title);

  // Executa a query
  if($stmt->execute()) {
    return true;
  }

  // Print mensagem de erro para caso o insert falhe
  printf("Error: $s.\n", $stmt->error);

  return false;
  }

  // Update do TODO
  public function update() {
    // Cria a query
    $query = 'UPDATE ' .
      $this->table . '
    SET
      todo_title = :todo_title
      WHERE
      id = :id';

  // Prepara o Statement
  $stmt = $this->conn->prepare($query);

  // Limpa a informação
  $this->todo_title = htmlspecialchars(strip_tags($this->todo_title));
  $this->id = htmlspecialchars(strip_tags($this->id));

  // Bind para os parâmetros
  $stmt-> bindParam(':todo_title', $this->todo_title);
  $stmt-> bindParam(':id', $this->id);

  // Execute a query
  if($stmt->execute()) {
    return true;
  }

  // Printa mensagem de erro caso o Update falhe
  printf("Error: $s.\n", $stmt->error);

  return false;
  }
  // Marca o todo como COMPLETO/INCOMPLETO
  public function checked() {
    // Cria a query
    $query = 'UPDATE ' .
      $this->table . '
    SET
      todo_done = :todo_done
      WHERE
      id = :id';

  // Prepara o Statement
  $stmt = $this->conn->prepare($query);

  // Limpa a informação
  $this->todo_done = htmlspecialchars(strip_tags($this->todo_done));
  $this->id = htmlspecialchars(strip_tags($this->id));

  // Bind para os parâmetros
  $stmt-> bindParam(':todo_done', $this->todo_done);
  $stmt-> bindParam(':id', $this->id);

  // Execute a query
  if($stmt->execute()) {
    return true;
  }

  // Printa mensagem de erro caso o Update falhe
  printf("Error: $s.\n", $stmt->error);

  return false;
  }
  
  // Delete o Todo
  public function delete() {
    // Cria a query
    $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

    // Prepara o Statement
    $stmt = $this->conn->prepare($query);

    // clean data
    $this->id = htmlspecialchars(strip_tags($this->id));

    // Bind para o parâmetro ID
    $stmt-> bindParam(':id', $this->id);

    // Executa a Query
    if($stmt->execute()) {
      return true;
    }

    // Printa mensagem de erro caso o DELETE falhe
    printf("Error: $s.\n", $stmt->error);

    return false;
    }
  }